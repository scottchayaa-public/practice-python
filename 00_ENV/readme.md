# Python 開發環境安裝

推薦使用 anaconda
https://www.anaconda.com/distribution/#download-section

安裝完後記得新增設定`環境變數`
```
C:\Users\__USER__\Anaconda3
C:\Users\__USER__\Anaconda3\Scripts
C:\Users\__USER__\Anaconda3\Library\bin
```

# 常用指令

```sh
conda --version
conda info --envs  # 查看 conda 目前 virtual env 環境

conda create -n py35 python=3.5 anaconda
conda create -n tensorflow 

conda activate tensorflow
```
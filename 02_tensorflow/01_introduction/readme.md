
# Prepare ENV : venv, vscode

install .venv
```sh
py -3 -m venv .venv

```

select python interpreter on vscode : `./vscode/setting.json`
```json
{
    "python.pythonPath": ".venv\\Scripts\\python.exe"
}
```



# Trobleshooting #1

```sh
(.venv) $ py helloworld.py 
2019-10-13 00:02:46.301239: I tensorflow/core/platform/cpu_feature_guard.cc:142] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2
Traceback (most recent call last):
  File "helloworld.py", line 21, in <module>
    sess = tf.Session()
AttributeError: module 'tensorflow' has no attribute 'Session'
```

https://github.com/tensorflow/tensorflow/issues/18538

```
pip install --upgrade tensorflow-gpu
```
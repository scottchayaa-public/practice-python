# VSCode Python 環境準備

[Install Python](https://www.python.org/downloads/)

設定環境變數 
C:\Users\__your_name__\AppData\Local\Programs\Python\Python37\Scripts

打開 vscode
安裝 python 擴充

```
$ py -V
Python 3.7.0
```

Install `pip`
Download [get-pip.py](https://bootstrap.pypa.io/get-pip.py)
```
py get-pip.py
```

若有出現提示要升級 pip 到最新, 則就按照他上面的指令進行升級



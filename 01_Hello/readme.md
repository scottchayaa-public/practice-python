
# Python tutorial
https://code.visualstudio.com/docs/python/python-tutorial

# Install venv and use packages
```py
# create virtual enviroment
py -3 -m venv .venv

# switch into venv
. .venv/Scripts/activate
or
source .venv/Scripts/activate
```

# Install the packages

```sh
(.venv) $ python -m pip install matplotlib

(.venv) $ pip list
Package         Version
--------------- -------
cycler          0.10.0
kiwisolver      1.1.0
matplotlib      3.1.1
numpy           1.17.2 
pip             19.0.3
pyparsing       2.4.2
python-dateutil 2.8.0
setuptools      40.8.0
six             1.12.0
```

# Select Interpreter in venv
確認你的 python 執行環境是否切換到 venv

.vscode/settings.json
```
{
    "python.pythonPath": ".venv\\Scripts\\python.exe"
}
```

![](./docs/1.png)


執行 demo.py

```sh
(.venv) $ python demo.py
```

![](./docs/2.png)


# Output requirements.txt

匯出安裝套件和版本
```py
(.venv) $ pip freeze > requirements.txt
```

未來要還原安裝套件時 : 
```py
(.venv) $ pip install -r requirements.txt
```


# References
- [pip workflow 管理 requirement.txt](http://pre.tir.tw/008/blog/output/pip-workflow-guan-li-requirementtxt.html)
